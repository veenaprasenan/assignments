package packages;

public class Swap {

	public static void main(String[] args) {
		System.out.println("Swapping Two Variables");
		System.out.println("-------------");
		int a=10,b=20;
		System.out.println("a="+a);
		System.out.println("b="+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("-------------");
		System.out.println("after swaping");
		System.out.println("-------------");
		System.out.println("a="+a);
		System.out.println("b="+b);
	}

}
