package packages;
import java.util.Scanner;
public class Palindrome {

	public static void main(String[] args) {
		
		System.out.println("Enter your string");
		System.out.println("-----------------");
		Scanner sc=new Scanner(System.in);
		String a=sc.next();
		char[] name=a.toCharArray();
		String b="";
		for(int i=name.length-1;i>=0;i--) {
			b=b.concat(String.valueOf(name[i]));
		}
		if(a.equalsIgnoreCase(b))
		{
			System.out.println("It is palindrome");

		}
		else
		{
			System.out.println("It is not palindrome");

		}
		sc.close();
	}

}
